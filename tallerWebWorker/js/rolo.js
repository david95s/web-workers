var i = "Lorem ipsum dffffffolor sit amet, consectetur adipisicing elit. Officia ab, hic esse, veniam debitis quaerat? Dolore sed, vero iusto magnam sapiente, assumenda autem architecto perspiciatis aliquam numquam possimus, natus asperiores.";
var info = {"nombre":"David",
			"apellido":"Sanchez",
			"edad":"20 años",
			"tema":"Tema a exponer Web Worker",
			"Semestre":"Septimo semestre",
			"clase":"Profundizacion web 1"}


function nombre() {
	postMessage(info.nombre);
	setTimeout("apellido()",2000);
}
function apellido () {
	postMessage(info.apellido);
	setTimeout("edad()",2000);
}
function edad () {
	postMessage(info.edad);
	setTimeout("tema()",2000);
}
function tema() {
	postMessage(info.tema);
	setTimeout("Semestre()",2000);
}
function Semestre () {
	postMessage(info.Semestre);
	setTimeout("clase()",2000);
}
function clase () {
	postMessage(info.clase);
	setTimeout("nombre()",2000);
}
nombre();
